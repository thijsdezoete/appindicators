import PyQt4
from PyQt4 import QtGui
from PyQt4 import *


class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        self.resize(250, 150)
        self.setWindowTitle('menubar')
        self.modal = False

        exit = QtGui.QAction( QtGui.QIcon('./ImageCache.png'), 'Exit', self )
        exit.setShortcut('Ctrl+Q')
        exit.setStatusTip('Exit application')
        self.connect(exit, QtCore.SIGNAL('triggered()'), QtCore.SLOT('close()'))

        menubar = self.menuBar()
        file = menubar.addMenu('File')
        file.addAction(exit)


if __name__ == '__main__':
    x = MainWindow()
