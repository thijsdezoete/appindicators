import requests
import indicate
# from indicate import Favicon
from urlparse import urlparse
# import sys
import os


class Reddit(object):
    def __init__(self, indicatr):
        self.url = 'http://www.reddit.com/'
        self.parent = indicatr
        self.parent.favicon.get('www.reddit.com')
        self.parent.set_top_icon('www.reddit.com')
        self.items = {}

    def click_link(self, link):
        os.system('open %s' % link)

    def process_item(self, item, url):
        base_url = str(urlparse(url).netloc)
        self.parent.favicon.get(base_url)
        self.items[item] = url
        self.parent.add_icon_to_menu(base_url,
            item,
            lambda: self.click_link(self.items[item])
        )

    def get_subreddit(self, sub):
        self.parent.add_to_menu((sub + ' subreddit').title(), lambda: 'hello')
        url = '{0}r/{1}/{2}.json?sort={2}&limit={3}'.format(self.url, sub, 'hot', '10')
        posts = requests.get(url).json()
        links = [(x['data']['title'][:12] + '...', str(x['data']['url'])) for x in posts['data']['children'] if x['kind'] == u't3']

        [self.process_item(item, url) for (item, url) in links]

    def run(self):
        self.parent.add_quit_btn()
        self.get_subreddit('python')
        self.parent.run()


if __name__ == '__main__':
    x = indicate.Indicatr()
    r = Reddit(indicatr=x)
    r.run()
