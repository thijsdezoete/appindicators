
import pprint
import os
import sys

platform = sys.platform
from PyQt4 import QtCore, QtGui  # , QtSvg

FOLDER = os.path.dirname(os.path.abspath(__file__))
IMAGE = FOLDER + '/ImageCache.png'

IMAGES = (
        FOLDER + '/ImageCache.png',
        FOLDER + '/apple.png',
        )


def quitCB():
    QtGui.QApplication.quit()


def aboutToShowCB():
    print 'about to show'


def hide_mac_dock_icon():
    try:
        import AppKit
        # https://developer.apple.com/library/mac/#documentation/AppKit/Reference/NSRunningApplication_Class/Reference/Reference.html
        NSApplicationActivationPolicyRegular = 0
        NSApplicationActivationPolicyAccessory = 1
        NSApplicationActivationPolicyProhibited = 2
        AppKit.NSApp.setActivationPolicy_(NSApplicationActivationPolicyProhibited)
    except: 
        # Don't do anything if we can't remove dock icon... 
        print 'Cant remove icon from dock. Install pyobjc to fix this'
        pass

def generate_menu(menu):
    from api import testing as my_plugin
    for (label, function) in my_plugin.get_entries():
        menu.addAction(label, function)


def swap_image(element):
    global IMG_ICONS
    (img1, img2) = IMG_ICONS
    IMG_ICONS = (img2, img1)
    print 'Setting new image on %s' % element
    element.setIcon(IMG_ICONS[0])
    try:
        element.show()
    except:  # Can't `show()` element..
        pass


def _chng_icon(action):
    swap_image(action)


if __name__ == '__main__':
    app = QtGui.QApplication([])

    if 'darwin' in platform:
        hide_mac_dock_icon()

    i = QtGui.QSystemTrayIcon()
    m = QtGui.QMenu()
    generate_menu(m)


    m.addAction('Change tray image', lambda: swap_image(i))
    # Use this to connect buttons in the future..
    # Qt.Core.SIGNAL:
    #QtCore.QObject.connect(m, QtCore.SIGNAL('aboutToShow()'), aboutToShowCB)
    i.setContextMenu(m)

    pm = QtGui.QPixmap(16, 16)
    IMG_ICONS = ()
    for image in IMAGES:
        tmp = QtGui.QIcon(image)
        tmp.addPixmap(pm)
        IMG_ICONS += (tmp,)

    hello = m.addAction(IMG_ICONS[1], "change this icon")
    QtCore.QObject.connect(hello, QtCore.SIGNAL('triggered()'), lambda: swap_image(hello))

    swap_image(i)
    # Add quit button at the bottom
    m.addAction('Quit', quitCB)
    app.exec_()

    #del svg #,pm avoid the paint device getting
    del i  # , icon          # deleted before the painter
    del app

