import requests
import indicate
# from indicate import Favicon
from urlparse import urlparse
# import sys
import os
from PyQt4 import QtGui
from PyQt4 import QtCore


class LoginWindow(QtGui.QDialog):
    def __init__(self, parent=None):
        super(LoginWindow, self).__init__()
        self.parent = parent
        self.setWindowTitle('New subreddit to follow')
        self.buttonLayout = QtGui.QVBoxLayout()
        self.username = QtGui.QLineEdit("username")
        self.password = QtGui.QLineEdit()
        # Display password as password
        self.password.setEchoMode(QtGui.QLineEdit.Password)

        self.buttonBox = QtGui.QGroupBox("Credentials")

        self.buttonLayout.addWidget(self.username)
        self.buttonLayout.addWidget(self.password)
        self.buttonBox.setLayout(self.buttonLayout)

        self.confirm = QtGui.QPushButton("&Confirm")
        self.confirm.clicked.connect(self.submit_form)

        self.username.setFocus()
        self.username.selectAll()

        self.buttonLayout.addWidget(self.confirm)
        self.setLayout(self.buttonLayout)

    def submit_form(self):
        username = str(self.username.text())
        password = str(self.password.text())
        print 'Login information saved.'
        self.close()
        self.parent.set_session_details(username, password)


class ChooseRedditWindow(QtGui.QDialog):
    def __init__(self, parent=None):
        super(ChooseRedditWindow, self).__init__()
        self.parent = parent
        self.setWindowTitle('New subreddit to follow')
        self.layout = QtGui.QGridLayout()

        self.setup_radio_buttons_sorting()
        self.setup_radiobuttons_click_action()

        self.confirm = QtGui.QPushButton("&Confirm")
        self.confirm.clicked.connect(self.submit_form)

        self.setup_edit_btn()
        self.layout.addWidget(QtGui.QLabel("What subreddit do you wish to be kept informed about?"), 0, 0, 1, 2)
        self.layout.addWidget(self.subreddit, 1, 0, 1, 2)

        self.links_only = QtGui.QCheckBox()
        self.layout.addWidget(QtGui.QLabel("Links only?"), 2, 0)
        self.layout.addWidget(self.links_only, 2, 1)

        # self.layout.addWidget(QtGui.QLabel("Which subcategory?"), 2, 0)
        self.layout.addWidget(self.buttonBox, 3, 0)
        self.layout.addWidget(self.click_buttonBox, 3, 1)

        self.layout.addWidget(self.confirm, 4, 0, 1, 2, QtCore.Qt.AlignCenter)

        self.setLayout(self.layout)
        self.subreddit.setFocus()
        self.subreddit.selectAll()

    def setup_edit_btn(self):
        self.subreddit = QtGui.QLineEdit("python")

        QtCore.QObject.connect(self.subreddit, QtCore.SIGNAL('returnPressed()'), self.submit_form)
        QtCore.QObject.connect(self, QtCore.SIGNAL('focusIn()'), self.subreddit.setFocus)

    def setup_radio_buttons_sorting(self):
        self.buttonBox = QtGui.QGroupBox("Subreddit type")
        self.buttonLayout = QtGui.QVBoxLayout()

        self.hot = QtGui.QRadioButton("Hot")
        self.hot.setChecked(True)
        self.new = QtGui.QRadioButton("New")
        self.ris = QtGui.QRadioButton("Rising")
        self.top = QtGui.QRadioButton("Top")

        self.buttons = [self.hot, self.new, self.ris, self.top]

        self.buttonLayout.addWidget(self.hot)
        self.buttonLayout.addWidget(self.new)
        self.buttonLayout.addWidget(self.ris)
        self.buttonLayout.addWidget(self.top)
        self.buttonBox.setLayout(self.buttonLayout)

    def setup_radiobuttons_click_action(self):
        self.click_buttonBox = QtGui.QGroupBox("Menu items link to")
        self.click_buttonLayout = QtGui.QVBoxLayout()

        self.link = QtGui.QRadioButton("Link")
        self.link.setChecked(True)
        self.thread = QtGui.QRadioButton("Thread")

        self.click_buttons = [self.link, self.thread]

        self.click_buttonLayout.addWidget(self.link)
        self.click_buttonLayout.addWidget(self.thread)
        self.click_buttonBox.setLayout(self.click_buttonLayout)

    def submit_form(self):
        # print 'Clickety-click!!!'
        sub = str(self.subreddit.text())
        # reddit_type = str(self.subreddit.text())
        # print 'Subreddit value: %s' % sub
        reddit_type = str([r.text() for r in self.buttons if r.isChecked()][0]).lower()
        # print 'Button checked: %s' % str(reddit_type)
        self.parent.add_subreddit(sub, reddit_type)
        # Close self
        self.close()
        # Renew entries in the menu
        self.parent.renew_entries()

        return


class Reddit(object):
    def __init__(self, indicatr):
        self.url = 'http://www.reddit.com/'
        self.parent = indicatr
        self.parent.favicon.get('www.reddit.com')
        self.parent.set_top_icon('www.reddit.com')
        self.session = requests.Session()
        self.items = {}
        self.subreddits = {}
        self.subredditwindow = ChooseRedditWindow(parent=self)
        self.loginwindow = LoginWindow(parent=self)

    def click_link(self, link):
        os.system('open %s' % link)

    def process_item(self, item, url, long_text=None):
        if long_text is None:
            long_text = item
        base_url = str(urlparse(url).netloc)
        if not item in self.items:
            self.items[item] = url
            self.parent.favicon.get(base_url)

        action = self.parent.add_icon_to_menu(
            base_url,
            item,
            lambda: self.click_link(self.items[item])
        )
        action.setToolTip(long_text)

    def set_session_details(self, username, password):
        self.username = username
        # self.session.auth = (username, password)
        t = self.session.post(self.url + 'api/login', data={'user': username, 'passwd': password})
        print 'If login not success: Show popup with error.'

    def add_refresh_btn(self):
        self.parent.add_to_menu('Renew entries', self.renew_entries)

    def add_new_reddit_btn(self):
        self.parent.add_to_menu('New subreddit', self._show_subreddit)

    def add_login_btn(self):
        self.parent.add_to_menu('Login', self._show_login)

    def add_subreddit(self, subreddit, subreddit_type='hot'):
        if not subreddit in self.subreddits:
            self.subreddits[subreddit] = subreddit_type

    def renew_entries(self):
        self.parent.reset_menu()
        self.add_login_btn()
        self.add_refresh_btn()
        self.add_new_reddit_btn()
        self.parent.add_separator()

        for sub in self.subreddits:
            self.get_subreddit(sub, self.subreddits[sub])
            self.parent.add_separator()

        self.parent.add_quit_btn()

    def get_subreddit(self, sub, reddit_type='hot'):
        json_url = '{0}r/{1}/{2}.json?sort={2}&limit={3}'.format(self.url, sub, reddit_type, '10')
        posts = requests.get(json_url).json()
        links = [(x['data']['title'][:20] + '...', str(x['data']['url']), x['data']['title']) for x in posts['data']['children'] if x['kind'] == u't3']
        # print links

        [self.process_item(item, url, long_text) for (item, url, long_text) in links]

    def _show_subreddit(self):
        # print 'Show that window!'
        # window = subredditwindow(parent=self)
        # self.show_window(window)
        self.subredditwindow.raise_()
        self.subredditwindow.activateWindow()
        self.parent.app.setActiveWindow(self.subredditwindow)
        self.parent.app.restoreOverrideCursor()
        self.subredditwindow.show()

    def show_window(self, window):
        window.show()
        window.raise_()
        window.activateWindow()
        self.parent.app.setActiveWindow(window)
        self.parent.app.restoreOverrideCursor()
        window.show()

    def _show_login(self):
        self.loginwindow.raise_()
        self.loginwindow.activateWindow()
        self.parent.app.setActiveWindow(self.loginwindow)
        self.parent.app.restoreOverrideCursor()
        self.loginwindow.show()

    def run(self, subreddit="python"):
        self.parent.reset_menu()
        # self.add_subreddit('python')
        # self.add_subreddit('php')
        self.renew_entries()
        # self.get_subreddit(subreddit)
        # self.parent.add_separator()
        # self.get_subreddit('php')

        self.parent.run()


if __name__ == '__main__':
    x = indicate.Indicatr()
    # x.hide_mac_dock_icon()
    r = Reddit(indicatr=x)
    r.run()
